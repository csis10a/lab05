# GridWorld Case Study

We will be following the exercises in the [student guide](http://www.collegeboard.com/prod_downloads/student/testing/ap/compsci_a/ap07_gridworld_studmanual_appends_v3.pdf). For Lab05, you will complete _Part 1: Observing and Experimenting with GridWorld_

JavaDocs for GridWorld can be found at [http://www.greenteapress.com/thinkapjava/javadoc/gridworld/](http://www.greenteapress.com/thinkapjava/javadoc/gridworld/)

The source code for GridWorld is also available at [http://hohonuuli.bitbucket.org/csis10a/ch05/GridWorldCode.zip](http://hohonuuli.bitbucket.org/csis10a/ch05/GridWorldCode.zip)

## Getting Started

1. Open a terminal, command prompt or PowerShell and change to your working directory (e.g. U:)
2. Grab a copy of the starter project using:  
`git clone https://bitbucket.org/csis10a/lab05.git`
3. If you have maven installed you can run the project:
- Windows: `run.bat`
- Everything else: `run.sh`
4. This lab is configured as a maven project. You can open it directly in most IDEs. If you're using BlueJ follow the instructions in the  [installation guide](http://www.collegeboard.com/prod_downloads/student/testing/ap/compsci_a/ap07_gridworld_installation_guide.pdf). 